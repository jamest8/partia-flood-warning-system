from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.station import MonitoringStation
from floodsystem.datafetcher import fetch_measure_levels
import datetime

stations = build_station_list()
update_water_levels(stations)

high_levels = []
moderate_levels = []
low_levels = []

severe_risk = set()
high_risk = set()
moderate_risk = set()
low_risk = set()

hstations = stations_highest_rel_level(stations, 20)


for station in stations_level_over_threshold(hstations, 0.9):
    high_levels.append(station)            ###A station has high levels if the latest level is close to or above the typical high (1.0)

for station in stations_level_over_threshold(hstations, 0.25):
    if station not in high_levels:
        moderate_levels.append(station)     ###A station has moderate levels if the latest level is below the typical high (1.0) but above 0.25 which is above the typical low (0). 

for station in hstations:
    if station not in high_levels and station not in moderate_levels and MonitoringStation.typical_range_consistent(station) == True:
        low_levels.append(station)    ##If a station has consistent data but is not in the high or moderate levels lists, it has low levels.



for station in high_levels:
    if MonitoringStation.typical_range_consistent(station) == True and station.latest_level != None:
        dt = 2
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        if levels != []:
            if station.latest_level >= (levels[-1])*1.2:
                severe_risk.add(station)
            elif station.latest_level < (levels[-1])*1.2 and station.latest_level >= (levels[-1])*0.8:
                high_risk.add(station)
            else:
                moderate_risk.add(station)

    ### If a station has high levels, the flood risk is considered severe if levels are rising, high if levels are remanining about constant and moderate if levels are falling.

for station in moderate_levels:
    if MonitoringStation.typical_range_consistent(station) == True and station.latest_level != None:
        dt = 2
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        if levels != []:
            if station.latest_level >= (levels[-1])*1.2:
                high_risk.add(station)
            elif station.latest_level < (levels[-1])*1.2 and station.latest_level >= (levels[-1])*0.8:
                moderate_risk.add(station)
            else:
                low_risk.add(station)


### If a station has moderate levels, the flood risk is considered high if levels are rising, moderate if levels are stationary and low if levels are falling.

for station in low_levels:
    if MonitoringStation.typical_range_consistent(station) == True and station.latest_level != None:
        dt = 2
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        if levels != []:
            if station.latest_level >= (levels[-1])*1.2:
                moderate_risk.add(station)
            else:
                low_risk.add(station)

### For stations with low levels, flood risk is considered to be moderate if levels are rising, and low if else.



print("SEVERE RISK:")
for i in severe_risk:
    print(i.name)
print("    ")

print("HIGH RISK:")
for i in high_risk:
    print(i.name)
print("    ")


print("MODERATE RISK:")
for i in moderate_risk:
    print(i.name)
print("    ")


print("LOW RISK:")
for i in low_risk:
    print(i.name)
print("    ")