from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list

stations = build_station_list()

rivers_with_station = sorted(rivers_with_station(stations))
print(rivers_with_station[:10])

stations_by_rivers = stations_by_river(stations)
print(sorted(stations_by_rivers["River Aire"]))
print(sorted(stations_by_rivers["River Cam"]))
print(sorted(stations_by_rivers["River Thames"]))
