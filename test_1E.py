from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

def test_river_by_station_number():
    stations = build_station_list()
    rivers_by_station = rivers_by_station_number(stations, 9)
    assert type(rivers_by_station) == list
    assert len(rivers_by_station) >= 9