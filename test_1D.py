from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list

def test_rivers_with_station():
    stations = build_station_list()
    rivers_ = sorted(rivers_with_station(stations))   #sorts into alphabetical order
    rivers_ = rivers_[:10] # returns first 10 rivers
    assert len(rivers_) == 10

def test_stations_by_river():
    stations = build_station_list()
    assert stations_by_river(stations) != None
