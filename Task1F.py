from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations

stations = build_station_list()
inconsistentdata = inconsistent_typical_range_stations(stations) #creates list of stations with inconsistent data
inconsistentstations = []
for item in inconsistentdata:
    inconsistentstations.append(item.name) #creates list including only the names of the stations
inconsistentstations.sort() #sorts into alphabetical order
print(inconsistentstations)