from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list

stations = build_station_list()
distancelist = stations_by_distance(stations, (52.2053, 0.1218)) #generates list of stations sorted by distance from Cambridge
closest10 = []
furthest10 = []
for i in range(10):  #creates lists of closest 10 and furthest 10 stations with correct formatting
    closest10.append((distancelist[i][0].name, distancelist[i][0].town, distancelist[i][1]))
    furthest10.append((distancelist[-i-1][0].name, distancelist[-i-1][0].town, distancelist[-i-1][1]))
print(closest10)
print(furthest10)