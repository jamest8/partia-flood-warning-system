from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import inconsistent_typical_range_stations



def test_stations_level_over_threshold():
    stations = build_station_list()
    update_water_levels(stations)

    inconsistent_typical_range_stations_ = []
    for station in inconsistent_typical_range_stations(stations):
       inconsistent_typical_range_stations_.append(station.name)

    for i in stations_level_over_threshold(stations, 0.8):
        assert i not in inconsistent_typical_range_stations_
