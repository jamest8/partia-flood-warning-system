from floodsystem.plot import plot_water_levels
from floodsystem.stationdata import build_station_list, update_water_levels
import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_highest_rel_level

stations = build_station_list()
update_water_levels(stations) #finds current water levels

top5 = stations_highest_rel_level(stations, 5)

for station in top5: #plots data using plot submodule
    dt = 10
    dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
    plot_water_levels(station, dates, levels)