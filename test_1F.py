from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations

def test_inconsistency():
    stations = build_station_list()
    inconsistentdata = inconsistent_typical_range_stations(stations) #creates list of stations with inconsistent data
    for item in inconsistentdata:
        assert item.typical_range_consistent() == False