# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
#from utils import sorted_by_key  # noqa
from haversine import haversine

def stations_by_distance(stations, p):
    distancelist = []
    for station in stations:
        distance = haversine(station.coord, p) #calculates distance using haversine function
        distancelist.append((station, distance))
    distancelist = sorted(distancelist, key=lambda x: x[1]) #sorts list by distance (second item in each tuple)
    return distancelist

def stations_within_radius(stations, centre, r):
    distancelist = stations_by_distance(stations, centre)
    enclosedstations = []
    for item in distancelist:
        if item[1] < r:
            enclosedstations.append(item)
    return enclosedstations



def rivers_with_station(stations):
    """Returns a list of the rivers which have a monitoring station on them."""
    rivers = set()
    for station in stations:
        rivers.add(station.river)
    return rivers



def stations_by_river(stations):
    """Returns a dictionary that maps river names (the ‘key’) to a list of station objects on the river"""
    station_objects = {}
        
    for station in stations:
        if station.river in station_objects:
            station_objects[station.river].append(station.name)
        else:
            station_objects[station.river] = [station.name]

    return station_objects



def rivers_by_station_number(stations, N):
    """Returns a list in descending order of the N rivers with the most stations on them."""
    rivers = set()
    most_stations = []

    for station in stations:
        rivers.add(station.river)

    for river in rivers:
        station_names = []
        for station in stations:
            if station.river == river:
                station_names.append(station.name)
        most_stations.append((river, len(station_names)))

    most_stations.sort(key=lambda tup: tup[1], reverse = True)
    most_stations_n = most_stations[:N]
    for i in range(N, len(most_stations)):
        if most_stations[i][1] == most_stations[N-1][1]:
            most_stations_n.append(most_stations[i])
    
    return most_stations_n
