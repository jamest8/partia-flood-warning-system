from .station import MonitoringStation
from .stationdata import build_station_list, update_water_levels


def stations_highest_rel_level(stations, N):
    """Returns a list of the N stations with the highest relative level currently."""
    stations_rel_level = []
    for station in stations:
        if station.relative_water_level() != None:
            stations_rel_level.append((station, station.relative_water_level()))
    stations_rel_level.sort(key=lambda tup: tup[1], reverse = True)
    stations_rel_level = stations_rel_level[:N]
    mstations = []
    for i in range(len(stations_rel_level)):
        mstations.append(stations_rel_level[i][0])
    return mstations



def stations_level_over_threshold(stations, tol):
    """Returns a list of the stations with a current relative water level above the tolerance/threshold."""
    update_water_levels(stations)
    stations_level_over_thresholds = []
    for station in stations:
        if MonitoringStation.typical_range_consistent(station) == True and station.latest_level != None:
            level = MonitoringStation.relative_water_level(station)
            if level > tol:
                stations_level_over_thresholds.append((station, level))
    stations_level_over_thresholds.sort(key=lambda tup: tup[1], reverse = True)
    stations_threshold = []
    for i in range(len(stations_level_over_thresholds)-1):
        stations_threshold.append(stations_level_over_thresholds[i][0])
    return stations_threshold



    
