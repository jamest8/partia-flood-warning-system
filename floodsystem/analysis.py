from floodsystem.datafetcher import fetch_measure_levels
import matplotlib as plt
import numpy as np

def polyfit(dates, levels, p):
    ##computes a least-squares fit of a polynomial of degree p to water level data

    times = plt.dates.date2num(dates)
    p_coeff = np.polyfit(times - times[0], levels, p)

    # Convert coefficient into a polynomial that can be evaluated
    # e.g. poly(0.3)
    poly = np.poly1d(p_coeff)

    return poly, (times - times[0])