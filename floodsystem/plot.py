from floodsystem.datafetcher import fetch_measure_levels
import matplotlib.pyplot as plt
import matplotlib
from datetime import datetime, timedelta
import numpy as np
from floodsystem.analysis import polyfit

def plot_water_levels(station, dates, levels):
    ## displays a plot of the water level against time for a station, including lines for the typical low and high levels
    plt.plot(dates, levels)

    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)
    plt.axhline(y=station.typical_range[0], color='r', linestyle='-') #adds line for upper and lower bounds of typical range
    plt.axhline(y=station.typical_range[1], color='r', linestyle='-')

    plt.tight_layout()
    plt.show()

def plot_water_level_with_fit(station, dates, levels, p):
    ##plots the water level data and the best-fit polynomial

    times = matplotlib.dates.date2num(dates)
    poly, d0 = polyfit(dates, levels, p)
    plt.plot(dates, levels) #plots actual data
    x1 = np.linspace(times[0], times[-1], len(poly(d0))) #creates x values with same dimension as y
    plt.plot(x1, poly(d0)) #plots polynomial fit

    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)
    plt.axhline(y=station.typical_range[0], color='r', linestyle='-') #adds line for upper and lower bounds of typical range
    plt.axhline(y=station.typical_range[1], color='r', linestyle='-')

    plt.tight_layout()
    plt.show()