from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list

stations = build_station_list()
cambridgestations = stations_within_radius(stations, (52.2053, 0.1218), 10) #produces liost of stations within 10km of Cambridge
cambridgenames = []
for item in cambridgestations:
    cambridgenames.append(item[0].name) #creates list including only the names of the stations
cambridgenames.sort() #sorts into alphabetical order
print(cambridgenames)

