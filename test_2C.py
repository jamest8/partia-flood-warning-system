from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import inconsistent_typical_range_stations

def test_stations_highest_rel_level():
    stations = build_station_list()
    update_water_levels(stations)
    N= 10
    stations_highest_rel_levels = stations_highest_rel_level(stations, N)
    assert len(stations_highest_rel_levels) == N
    inconsistent_typical_range_stations_ = []
    for station in inconsistent_typical_range_stations(stations):
       inconsistent_typical_range_stations_.append(station.name)
    
    for station in stations_highest_rel_levels:
       assert station not in inconsistent_typical_range_stations_
