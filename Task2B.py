from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.flood import stations_level_over_threshold
from floodsystem.station import MonitoringStation

stations = build_station_list()
update_water_levels(stations)

x = stations_level_over_threshold(stations, 0.8)
for i in range(len(x)-1):
    station = x[i]
    print(station.name, MonitoringStation.relative_water_level(station))
