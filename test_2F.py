from floodsystem.datafetcher import fetch_measure_levels
import matplotlib.pyplot as plt
import matplotlib
from datetime import datetime, timedelta
import numpy as np
from floodsystem.analysis import polyfit
from floodsystem.stationdata import build_station_list, update_water_levels
import datetime
from floodsystem.flood import stations_highest_rel_level

def test_correct_plot(): #checks each x-value has corresponding y-value
    stations = build_station_list()
    update_water_levels(stations)
    top5 = stations_highest_rel_level(stations, 5)

    for station in top5:
        dt = 2
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        times = matplotlib.dates.date2num(dates)
        poly, d0 = polyfit(dates, levels, 4)
        x1 = np.linspace(times[0], times[-1], len(poly(d0)))
        assert len(x1) == len(poly(d0))