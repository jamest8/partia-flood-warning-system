from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list

def test_distances(): #checks two distances from list against known values
    stations = build_station_list()
    distancelist = stations_by_distance(stations, (52.2053, 0.1218))
    assert round(distancelist[0][1], 3) == 0.840 #rounds number to ensure same number of decimal places as reference
    assert round(distancelist[-1][1], 3) == 467.534