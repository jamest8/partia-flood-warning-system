###ORIGINAL TASK CODE

from floodsystem.plot import plot_water_levels
from floodsystem.stationdata import build_station_list, update_water_levels
import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_highest_rel_level

def test_E(): #checks each x-value has a y-value on plot
    stations = build_station_list()
    update_water_levels(stations)
    top5 = stations_highest_rel_level(stations, 5)

    for station in top5:
        dt = 10
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        assert len(dates) == len(levels)